# Face Verification

Deepface is a lightweight face recognition and facial attribute analysis (age, gender, emotion and race) framework for python. It is a hybrid face recognition framework wrapping state-of-the-art models: VGG-Face, Google FaceNet, OpenFace, Facebook DeepFace, DeepID, ArcFace and Dlib.

Face Verification - ![DEMO](demo.mp4)

Verification function under the deepface interface offers to verify face pairs as same person or different persons. You should pass face pairs as array instead of calling verify function in a for loop for the best practice. This will speed the function up dramatically and reduce the allocated memory.


## Technologies used
- Python3
- DeepFace API
- JavaScript API
- Google Colab

## Acknowledgements

- FaceRecognition API - [DeepFace](https://pypi.org/project/deepface/) documentation 
- JavaScript API - This [notebook](https://colab.research.google.com/notebooks/snippets/advanced_outputs.ipynb) helped me script camera accessing part in colab.

